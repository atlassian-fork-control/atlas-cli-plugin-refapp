import { write } from 'fs';

const noop = () => null;

export const log = (msg: string) => {
  if (process.env.ATLAS_LOG_OUTPUT) {
    const fd = Number(process.env.ATLAS_LOG_OUTPUT.replace(/^fd/, ''));
    write(fd, JSON.stringify({ msg }) + '\n', noop);
  } else {
    console.error('It looks like the executable has been started independently from Atlas CLI');
  }
};
