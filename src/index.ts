import * as yargs from 'yargs';
import { log } from './log';
const { version } = require('../package');

const argv = yargs
  .option('debug', {
    boolean: true,
    describe: 'Show environment variables and process arguments',
  })
  .option('log', {
    boolean: true,
    describe: 'Send sample log line to Splunk through Atlas CLI',
  })
  .usage(`Usage: $0 --help`)
  .version(version)
  .strict()
  .parse(process.argv);

if (argv.debug) {
  const output = JSON.stringify({
    env: process.env,
    args: argv._,
  }, null, 2);

  console.log(output);
} else if (argv.log) {
  log('This is a sample log item sent by atlas-cli-plugin-refapp');
} else {
  console.error('No option selected');
  process.exit(1);
}
